package sse659.project1;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Known input, expected output<br>
 * In read-only collection for use in JUnit test assertions
 * 
 * @author Hatfield, Kevin
 */

public class AllTests
{
	private Map<String,Map<Integer,Integer>> _feralColony =
			
			new HashMap<String,Map<Integer,Integer>>(); 
	
	AllTests()
	{
		Map<Integer,Integer> populationYear = new HashMap<Integer,Integer>();
		
		populationYear.put(1, 37);
		populationYear.put(2, 37);
		populationYear.put(3, 74);
		populationYear.put(4, 74);
		populationYear.put(5, 148);
		populationYear.put(6, 148);
		
		_feralColony.put("population", populationYear);
		
	}//constructor
	
	Map<String,Map<Integer,Integer>> get_feralColony()
	{
		return Collections.unmodifiableMap(_feralColony);
		
	}//method
	
}//class

