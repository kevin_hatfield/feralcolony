package sse659.project1;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

/**
 * CatCheck10 does not work
 * 
 * <ul>
 * <li> Test class showing cat population for given year
 * <li> Test using known input, expected output
 * <li> Starting population 37, doubles every odd year thereafter
 * </ul>
 * 
 * TDD: Failed refactor attempt example<br><br> 
 * 
 * Moving conditional logic branches, loop logic to methods<br><br>
 * 
 * Fails tests
 
 * @author Hatfield, Kevin
 */

public class CatCheck10
{
	/**
	 * fixed input and output data for tests
	 */
	
	Map<String,Map<Integer,Integer>> _feralColony = 
			new HashMap<String,Map<Integer,Integer>>();  
			
	Map<Integer,Integer> _cats4Yr = new HashMap<Integer,Integer>();

	
	@Before
	public void setUp() throws Exception 
	{
		_feralColony = new AllTests().get_feralColony();
		
		_cats4Yr = _feralColony.get("population");
		
	}//method

	
	/**
	 * run the test 
	 */
	
	@Test
	public void test() 
	{
		
				
		//testColonySizeEachYear();
		
	}//method

	
	/**
	 * refactor the test run
	 * 
	 * test new catCount method
	 */
	
	private void testColonySizeEachYear()
	{

		FeralCatColony wildCats = new FeralCatColony(_cats4Yr.get(1),1);
		
		assertEquals( _cats4Yr.get(1), (Integer) wildCats.catCount(1) ); 
		assertEquals( _cats4Yr.get(2), (Integer) wildCats.catCount(2) );
		assertEquals( _cats4Yr.get(3), (Integer) wildCats.catCount(3) );
		assertEquals( _cats4Yr.get(4), (Integer) wildCats.catCount(4) );
		assertEquals( _cats4Yr.get(5), (Integer) wildCats.catCount(5) );
		assertEquals( _cats4Yr.get(6), (Integer) wildCats.catCount(6) );
		
	}//method
	
	/**
	 * <ul>
	 * <li>Show feral cat colony population for a year and starting population
	 * 
	 * <li>Default start year of '1'
	 * </ul>
	 * 
	 * @author Hatfield, Kevin
	 *
	 */
	
	class FeralCatColony
	{
		/* 
		 * instance variables 
		 */
		
		private int _yearFounded = 1;	// @TODO year validation
		
		private Integer _populationStartSize = 0;
		
		private Map<Integer,Integer> _catColony = 
				new HashMap<Integer,Integer>(); 

		private Map<Integer,Integer> _emptyCatColony;
		
		/* 
		 * constructors 
		 */
		
		FeralCatColony(Integer populationStartSize)
		{
			initInstance();
			
			set_populationStartSize(populationStartSize);
			
		}//constructor

		
		FeralCatColony(Integer populationStartSize, Integer startYear)
		{
			initInstance();

			set_yearFounded(startYear);
			
			set_populationStartSize(populationStartSize);
			
		}//constructor

		
		private void initInstance()
		{
			_yearFounded = 1;
			_populationStartSize = 0;
			_catColony = new HashMap<Integer,Integer>();
			
			_emptyCatColony = new HashMap<Integer,Integer>(); 
			
			assertTrue("Failed to initizalize cat colony",
					_emptyCatColony.equals(_catColony));
			
		}//method
		
		
		/* 
		 * encapsulation methods 
		 */

		private Map<Integer,Integer> get_emptyCatColony()
		{
			return _emptyCatColony;
			
		}//method
		
		
		private void set_populationStartSize(Integer sizeIn)
		{
			_populationStartSize = Math.abs(sizeIn);
			
		}//method

		
		private void set_yearFounded(Integer yearIn)
		{
			_yearFounded = yearIn;
			
		}//method

		
		private Integer get_yearFounded()
		{
			return _yearFounded;
			
		}//method

		
		private Integer get_populationStartSize()
		{
			return _populationStartSize;
			
		}//method

		
		/**
		 * refactor: add explaining variable
		 * 
		 * @return age of cat colony in years
		 */
		
		private Integer get_catColonyAge()
		{
			int yearsOld = _catColony.size();
			
			return yearsOld > 0 ? yearsOld : 0;
			
		}//method
		
		
		private Map<Integer,Integer> get_catColony() 
		{
			assertFalse("Colony uninitialized", 
					
					_catColony.equals(_emptyCatColony) );
			
			return Collections.unmodifiableMap(_catColony);
			
		}//method

		
		/** refactor: additional signature to accommodate data class 
		 * @throws Exception */
		
		private void set_catColony(colonyYear yearIn, Integer sizeIn) 
				throws Exception
		{
			if( yearIn.isBogus() )
			{ 
				throw new Exception( "Year unknown " + yearIn.asInt() );
			}//if
			
			set_catColony(yearIn.asInt(), sizeIn);
			
		}//method
		
		
		private void set_catColony(Integer yearIn, Integer sizeIn)	// cat count 
		{
			_catColony.put(yearIn, sizeIn);
			
		}//method
		

		/**
		 * @deprecated superseded by doubleCats()
		 */
		@SuppressWarnings("unused")
		private void doublePopulation()
		{
			doubleCats();
			
		}//method
		
		
		private Integer doubleCats()
		{
			Integer colonyAge = get_catColonyAge();
			
			assertTrue("Colony age is not null", colonyAge != null);

			assertTrue("Colony age > 0", colonyAge > 0);
			
			Integer cats = get_catColony().get( colonyAge );

			assertTrue("cat population > 0", cats > 0);
			
			return cats * 2;
			
		}//method
		
		
		private Integer firstCats()
		{
			Integer colonyAge = get_yearFounded();
			
			return get_populationStartSize();
			
		}//method
		
		
		private Integer sameCats()
		{
			Integer thisYear = get_catColonyAge();
			
			colonyYear lastYear = new colonyYear( thisYear - 1 );
			
			Integer catsLastYr = -1;
			
			if( lastYear.isValid() ) catsLastYr =
					
					get_catColony().get( lastYear.asInt() );

			assertTrue("Previous year population defined, year:" + 
					lastYear.asInt(), catsLastYr != null);
			
			if( lastYear.isBogus() ) catsLastYr = get_populationStartSize();
				
			assertTrue("Previous year population > zero", catsLastYr > 0);
			
			return catsLastYr;
			
		}//method
		
		
		/*
		 * functional methods 
		 */
		
		/**
		 * catCount: cat census for given year
		 * 
		 * @param yearIn Integer: greater or equal to cat colony's founding year
		 * @return Integer: cat colony population, -1 if year is invalid
		 */

		protected Integer catCount(Integer yearIn)
		{
			/** 
			 * refactor: move data property into predicate method
			 *  
			 * boolean futureYear = get_catColonyAge() < yearIn ? true : false;
			 */
			
			colonyYear colonyYearIn = new colonyYear(yearIn);
			
			if( colonyYearIn.isValid() && colonyYearIn.isFuture() )
			{ 
				setColonyAge(yearIn);
				
			}//if
			
			assertTrue("Colony of size " + get_catColony().size() +
					
					" lacks year " + yearIn, 
					
					get_catColony().containsKey(yearIn));
			
			return colonyYearIn.isValid() ? get_catColony().get(yearIn) : -1;
			
		}//method
		
		
		/**
		 * setColonyAge
		 * 
		 * @param Integer: year >= colony founding year 
		 * 
		 * @return boolean: T/F successful cat population prediction for yearIn
		 */
		
		private boolean setColonyAge(Integer yearIn)
		{			
			colonyYear colonyAgeYr = new colonyYear(yearIn);
			
			if( colonyAgeYr.isBogus() ) return false;
			
			// refactor: move conditional logic to methods
			
			for( int colonyAge = get_catColonyAge(); 
					 colonyAge <= yearIn; 
					 colonyAge++ )
			{
				
				colonyAgeYr = new colonyYear(colonyAge);
				
				// special case: 1st year
				
				if( colonyAgeYr.isFirstYear() )
				{
					/* refactor: move from conditional
					 * 
					 * colonyAge = get_yearFounded();
					 *
					 * set_catColony( colonyAge, get_populationStartSize() );
					 */
					
					set_catColony( colonyAge, firstCats() );
					
				}
				else if( colonyAgeYr.isEvenYear() )
				{
					// equal to previous year, no growth
					
					/* refactor: move from conditional
					 * 
					 * set_catColony( colonyAge, 
					 *		
					 *		get_catColony().get( colonyAge - 1 ) );
					 */
					
					set_catColony( colonyAge, sameCats() );
					
				}
				else if( colonyAgeYr.isOddYear() )
				{
					// odd years, population doubles
					
					/** refactor: move conditional logic to method */	
					
					/*
					 * set_catColony( colonyAge,
					 *	
					 *		get_catColony().get( colonyAge - 1 ) * 2 );					
					 */
					
					set_catColony( colonyAge, doubleCats() );
					
				}//if
				
			}//for
			
			assertTrue("Colony population prediction failed for year " + yearIn, 
					get_catColony().containsKey(yearIn));
			
			return get_catColony().containsKey(yearIn);
			
		}//method
	
		
		/**
		 * data class instead of primitive for Year
		 */
		
		private class colonyYear
		{
			private Integer _yearInt = 0;
			
			colonyYear(int yearIn)
			{
				set_yearInt(yearIn);
				
			}//constructor

			//encapsulation methods
			
			private void set_yearInt(Integer yearIn)
			{
				_yearInt = yearIn;
				
			}//method

			
			private Integer get_yearInt()
			{
				return _yearInt;
				
			}//method
			
			
			//predicate methods
			
			/** reduce duplication: utilize existing method */
			
			private boolean isValid()
			{
				//return validColonyYear( get_yearInt() );	
			
				return get_yearInt() >= get_yearFounded() ? true : false;
				
			}//method


			/**
			 * refactor: add additional method rather than using else or not
			 * in conditional login within method
			 */
			
			private boolean isBogus()
			{
				return !isValid();
				
			}//method
			
			
			/** 
			 * refactor: removal of conditional branch 
			 *
			 * if( colonyAge <= get_yearFounded() )
			 * 
			 */
			
			private boolean isFirstYear()
			{
				return get_yearInt() <= get_yearFounded() ? true : false;
						
			}//method

			
			/**
			 * refactor: removal of conditional branch
			 * 
			 * else if(colonyAge % 2 == 0) 
			 */
			
			private boolean isEvenYear()
			{
				return get_yearInt() % 2 == 0 ? true : false;
				
			}//method
			
			
			/**
			 * refactor: add additional method rather than using else or not
			 * in conditional login within method
			 */

			private boolean isOddYear()
			{
				return !isEvenYear();
				
			}//method

			
			private boolean isFuture()
			{
				return get_catColonyAge() < get_yearInt() ? true : false;
				
			}//method

			
			private Integer asInt()
			{
				return get_yearInt();
				
			}//method
			
		}//class
		
	}//class
	
	

	
}//class
