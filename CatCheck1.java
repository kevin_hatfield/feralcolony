package sse659.project1;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

/**
 * CatCheck1<br><br>
 * 
 * Minimal implementation; returns literals to meet assertions
 * @author Hatfield, Kevin
 */

public class CatCheck1
{

	Map<String,Map<Integer,Integer>> _feralColony = 
			new HashMap<String,Map<Integer,Integer>>();  
			
	Map<Integer,Integer> _cats4Yr = new HashMap<Integer,Integer>();
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	
	@Before
	public void setUp() throws Exception 
	{
		_feralColony = new AllTests().get_feralColony();
		
		_cats4Yr = _feralColony.get("population");
		
		//System.out.println("Yr1 : " + _catsForYr.get(1));
		
	}//method

	
	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void test() 
	{
		//fail("Not yet implemented");
		
		checkColonySizeEachYear();
		
	}//method
	
	private void checkColonySizeEachYear()
	{

		assertEquals( _cats4Yr.get(1), (Integer) 37 );
		assertEquals( _cats4Yr.get(2), (Integer) 37 );		
		assertEquals( _cats4Yr.get(3), (Integer) 74 );		
		assertEquals( _cats4Yr.get(4), (Integer) 74 );
		assertEquals( _cats4Yr.get(5), (Integer) 148 );
		assertEquals( _cats4Yr.get(6), (Integer) 148 );
		
	}//method
	
}//class
