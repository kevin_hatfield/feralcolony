package sse659.project1;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

/**
 * CatCheck15<br><br>
 * 
 * parameter eliminated from samePopulation() method
 * 
 * <ul>
 * <li> Test class showing cat population for given year
 * <li> Test using known input, expected output
 * <li> Starting population 37, doubles every odd year thereafter
 * </ul>
 * 
 * @author Hatfield, Kevin
 */

public class CatCheck15
{
	/**
	 * fixed input and output data for tests
	 */
	
	Map<String,Map<Integer,Integer>> _feralColony = 
			new HashMap<String,Map<Integer,Integer>>();  
			
	Map<Integer,Integer> _cats4Yr = new HashMap<Integer,Integer>();

	
	@Before
	public void setUp() throws Exception 
	{
		_feralColony = new AllTests().get_feralColony();
		
		_cats4Yr = _feralColony.get("population");
		
	}//method

	
	/**
	 * run the test 
	 */
	
	@Test
	public void test() 
	{
		//checkColonySizeEachYear();
		
		testColonySizeEachYear();
		
	}//method

	
	/**
	 * refactor the test run
	 * 
	 * test new catCount method
	 */
	
	private void testColonySizeEachYear()
	{

		FeralCatColony wildCats = new FeralCatColony(_cats4Yr.get(1),1);
		
		assertEquals( _cats4Yr.get(1), (Integer) wildCats.catCount(1) ); 
		assertEquals( _cats4Yr.get(2), (Integer) wildCats.catCount(2) );
		assertEquals( _cats4Yr.get(3), (Integer) wildCats.catCount(3) );
		assertEquals( _cats4Yr.get(4), (Integer) wildCats.catCount(4) );
		assertEquals( _cats4Yr.get(5), (Integer) wildCats.catCount(5) );
		assertEquals( _cats4Yr.get(6), (Integer) wildCats.catCount(6) );
		
	}//method
	
	/**
	 * <ul>
	 * <li>Show feral cat colony population for a year and starting population
	 * 
	 * <li>Default start year of '1'
	 * </ul>
	 * 
	 * @author Hatfield, Kevin
	 *
	 */
	
	class FeralCatColony
	{
		/* 
		 * instance variables 
		 */
		
		private int _yearFounded = 1;	// @TODO year validation
		
		private Integer _populationStartSize = 0;
		
		private Map<Integer,Integer> _catColony = 
				new HashMap<Integer,Integer>(); 

		
		/* 
		 * constructors 
		 */
		
		FeralCatColony(Integer populationStartSize)
		{
			initInstance();
			
			set_populationStartSize(populationStartSize);
			
		}//constructor

		
		FeralCatColony(Integer populationStartSize, Integer startYear)
		{
			initInstance();

			set_yearFounded(startYear);
			
			set_populationStartSize(populationStartSize);
			
		}//constructor

		
		private void initInstance()
		{
			_yearFounded = 1;
			_populationStartSize = 0;
			_catColony = new HashMap<Integer,Integer>();
			
		}//method
		
		
		/* 
		 * encapsulation methods 
		 */
		
		private void set_populationStartSize(Integer sizeIn)
		{
			_populationStartSize = Math.abs(sizeIn);
			
		}//method

		
		private void set_yearFounded(Integer yearIn)
		{
			_yearFounded = yearIn;
			
		}//method

		
		private Integer get_yearFounded()
		{
			return _yearFounded;
			
		}//method

		
		private Integer get_populationStartSize()
		{
			return _populationStartSize;
			
		}//method

		
		/**
		 * refactor: add explaining variable
		 * 
		 * @return age of cat colony in years
		 */
		
		private Integer get_catColonyAge()
		{
			int yearsOld = _catColony.size();
			
			return yearsOld;
			
		}//method
		
		
		private Map<Integer,Integer> get_catColony() 
		{
			return Collections.unmodifiableMap(_catColony);
			
		}//method

		
		private void set_catColony(Integer yearIn, Integer sizeIn)	// add cats 
		{
			_catColony.put(yearIn, sizeIn);
			
		}//method

		
		private void set_catColony(colonyYear yearIn, Integer sizeIn)
		{
			_catColony.put(yearIn.get_yearInt(), sizeIn);
			
		}//method
		
		
		private boolean initialPopulation()
		{			
			Integer startYear = get_yearFounded();
			
			set_catColony( startYear, get_populationStartSize() );

			
			boolean result = get_catColony().get( get_catColonyAge() ) 
					
					== get_populationStartSize();
			
			assertTrue("Failed to set initial population", result);
			
			return result;
			
		}//method
		
		
		private boolean samePopulation()
		{
			colonyYear currentYear = new colonyYear();
			
			if( get_catColonyAge() == get_yearFounded() )
			{
				currentYear = new colonyYear( currentYear.modify(+1) );
				
			}//if
						
			Integer lastYear = currentYear.modify(-1);
			
			Integer catsLastYear = get_catColony().get( lastYear );
			
			set_catColony( currentYear, catsLastYear );
			
			
			boolean result = get_catColony().get( get_catColonyAge() ) ==
					
					get_catColony().get( lastYear );

			assertTrue("Setting new year to previous year population failed", 
					
					result);

			return result;
			
		}//method
		

		private boolean doublePopulation()
		{
			Integer colonyAge = get_catColonyAge();
						
			Integer cats = get_catColony().get( colonyAge );
			
			assertTrue("cat population > 0", cats > 0);
			
			set_catColony(colonyAge+1, cats * 2);
			
			
			boolean result = get_catColony().get( get_catColonyAge() ) ==
			
					get_catColony().get( colonyAge ) * 2;
			
			assertTrue("Failed to double population", result);
			
			return result; 
					
		}//method
		
		
		/*
		 * functional methods 
		 */
		
		/**
		 * catCount: cat census for given year
		 * 
		 * @param yearIn Integer: greater or equal to cat colony's founding year
		 * @return Integer: cat colony population, -1 if year is invalid
		 */

		protected Integer catCount(Integer yearIn)
		{			
			colonyYear catYear = new colonyYear(yearIn);
			
			if( catYear.isValid() && catYear.isFuture() || 
					
					catYear.isFirstYear() )
			{ 
				setColonyAge(yearIn);
				
			}//if
			
			assertTrue( "Year not found " + yearIn, catYear.isKnown() );
			
			return catYear.isKnown() ? 
					
					get_catColony().get(yearIn) : -1;
			
		}//method
		
		
		/**
		 * setColonyAge
		 * 
		 * @param Integer: year >= colony founding year 
		 * 
		 * @return boolean: T/F successful cat population prediction for yearIn
		 */
		
		private boolean setColonyAge(Integer yearIn)
		{			
			colonyYear colonyAgeYr = new colonyYear(yearIn);
			
			if( colonyAgeYr.isBogus() ) return false;
			
			// refactor: move conditional logic to methods
			
			for( int colonyAge = get_catColonyAge(); 
					 colonyAge <= yearIn; 
					 colonyAge++ )
			{
				colonyAgeYr = new colonyYear(colonyAge);
				
				//refactor: loop parameter now unused
				
				// special case: 1st year
				
				if( colonyAgeYr.isFirstYear() )
				{
					initialPopulation();
					
				}
				else if( colonyAgeYr.isEvenYear() )
				{
					samePopulation();

				}
				else if( colonyAgeYr.isOddYear() )
				{
					doublePopulation();
					
				}//if
				
			}//for
			
			return true;
			
		}//method
	
		
		/**
		 * data class instead of primitive for Year
		 */
		
		private class colonyYear
		{
			private Integer _yearInt = 0;
			
			colonyYear(int yearIn)
			{
				set_yearInt(yearIn);
				
			}//constructor
			
			
			colonyYear()
			{
				Integer currentAge = get_catColonyAge();
				
				set_yearInt( currentAge );
				
			}//no-arg constructor

			//encapsulation methods
			
			private void set_yearInt(Integer yearIn)
			{
				_yearInt = yearIn;
				
			}//method

			
			private Integer get_yearInt()
			{
				return _yearInt;
				
			}//method
			
			
			//predicate methods
			
			/** reduce duplication: utilize existing method */
			
			private boolean isValid()
			{
				//return validColonyYear( get_yearInt() );	
			
				return get_yearInt() >= get_yearFounded() ? true : false;
				
			}//method


			/**
			 * refactor: add additional method rather than using else or not
			 * in conditional login within method
			 */
			
			private boolean isBogus()
			{
				return !isValid();
				
			}//method
			
			
			/** 
			 * refactor: removal of conditional branch 
			 *
			 * if( colonyAge <= get_yearFounded() )
			 * 
			 */
			
			private boolean isFirstYear()
			{
				return get_yearInt() <= get_yearFounded() ? true : false;
						
			}//method

			
			/**
			 * refactor: removal of conditional branch
			 * 
			 * else if(colonyAge % 2 == 0) 
			 */
			
			private boolean isEvenYear()
			{
				return get_yearInt() % 2 == 0 ? true : false;
				
			}//method
			
			
			/**
			 * refactor: add additional method rather than using else or not
			 * in conditional login within method
			 */

			private boolean isOddYear()
			{
				return !isEvenYear();
				
			}//method
			
			
			private boolean isFuture()
			{
				boolean postColonyEpoch = get_yearInt() > get_yearFounded();

				/** 
				 * refactor: replaced Map contains method with predicate 
				 * method isKnown() 
				 */
				
				return postColonyEpoch && !isKnown();
				
			}//method

			
			private boolean isKnown()
			{
				return get_catColony().containsKey( get_yearInt() );
				
			}//method
			
			
			private Integer modify(Integer changeBy)
			{
				return Math.abs( get_yearInt() + changeBy );
				
			}//method
			
		}//class
		
	}//class
	
	

	
}//class
