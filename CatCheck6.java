package sse659.project1;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

/**
 * CatCheck6
 *
 * Deprecate methods which have been replaced, renamed<br>
 * Reduce duplicated logic
 *
 * <ul>
 * <li> Test class showing cat population for given year
 * <li> Test using known input, expected output
 * <li> Starting population 37, doubles every odd year thereafter
 * </ul>
 * 
 * @author Hatfield, Kevin
 */

public class CatCheck6
{
	/**
	 * fixed input and output data for tests
	 */
	
	Map<String,Map<Integer,Integer>> _feralColony = 
			new HashMap<String,Map<Integer,Integer>>();  
			
	Map<Integer,Integer> _cats4Yr = new HashMap<Integer,Integer>();

	
	@Before
	public void setUp() throws Exception 
	{
		_feralColony = new AllTests().get_feralColony();
		
		_cats4Yr = _feralColony.get("population");
		
	}//method

	
	/**
	 * run the test 
	 */
	
	@Test
	public void test() 
	{
		//checkColonySizeEachYear();
		
		testColonySizeEachYear();
		
	}//method

	
	/**
	 * checkColonySizeEachYear
	 * 
	 * <br>test: compare fixed input and output data against class being tested
	 */
	
	private void checkColonySizeEachYear()
	{

		FeralCatColony wildCats = new FeralCatColony(_cats4Yr.get(1),1);
		
		assertEquals( _cats4Yr.get(1), (Integer) wildCats.getSize(1) ); 
		assertEquals( _cats4Yr.get(2), (Integer) wildCats.getSize(2) );
		assertEquals( _cats4Yr.get(3), (Integer) wildCats.getSize(3) );
		assertEquals( _cats4Yr.get(4), (Integer) wildCats.getSize(4) );
		assertEquals( _cats4Yr.get(5), (Integer) wildCats.getSize(5) );
		assertEquals( _cats4Yr.get(6), (Integer) wildCats.getSize(6) );
		
	}//method

	/**
	 * refactor the test run
	 * 
	 * test new catCount method
	 */
	
	private void testColonySizeEachYear()
	{

		FeralCatColony wildCats = new FeralCatColony(_cats4Yr.get(1),1);
		
		assertEquals( _cats4Yr.get(1), (Integer) wildCats.catCount(1) ); 
		assertEquals( _cats4Yr.get(2), (Integer) wildCats.catCount(2) );
		assertEquals( _cats4Yr.get(3), (Integer) wildCats.catCount(3) );
		assertEquals( _cats4Yr.get(4), (Integer) wildCats.catCount(4) );
		assertEquals( _cats4Yr.get(5), (Integer) wildCats.catCount(5) );
		assertEquals( _cats4Yr.get(6), (Integer) wildCats.catCount(6) );
		
	}//method
	
	/**
	 * <ul>
	 * <li>Show feral cat colony population for a year and starting population
	 * 
	 * <li>Default start year of '1'
	 * </ul>
	 * 
	 * @author Hatfield, Kevin
	 *
	 */
	
	class FeralCatColony
	{
		/* 
		 * instance variables 
		 */
		
		private int _yearFounded = 1;	// @TODO year validation
		
		private Integer _populationStartSize = 0;
		
		private Map<Integer,Integer> _catColony = 
				new HashMap<Integer,Integer>(); 

		
		/* 
		 * constructors 
		 */
		
		FeralCatColony(Integer populationStartSize)
		{
			initInstance();
			
			set_populationStartSize(populationStartSize);
			
		}//constructor

		
		FeralCatColony(Integer populationStartSize, Integer startYear)
		{
			initInstance();

			set_yearFounded(startYear);
			
			set_populationStartSize(populationStartSize);
			
		}//constructor

		
		void initInstance()
		{
			_yearFounded = 1;
			_populationStartSize = 0;
			_catColony = new HashMap<Integer,Integer>();
			
		}//method
		
		
		/* 
		 * encapsulation methods 
		 */
		
		private void set_populationStartSize(Integer sizeIn)
		{
			_populationStartSize = Math.abs(sizeIn);
			
		}//method

		
		private void set_yearFounded(Integer yearIn)
		{
			_yearFounded = yearIn;
			
		}//method

		
		private Integer get_yearFounded()
		{
			return _yearFounded;
			
		}//method

		
		private Integer get_populationStartSize()
		{
			return _populationStartSize;
			
		}//method

		
		/**
		 * Refactor: add explaining variable
		 * 
		 * @return age of cat colony in years
		 */
		
		private Integer get_catColonyAge()
		{
			int yearsOld = _catColony.size();
			
			return yearsOld;
			
		}//method
		
		
		private Map<Integer,Integer> get_catColony() 
		{
			return Collections.unmodifiableMap(_catColony);
			
		}//method

		
		private void set_catColony(Integer yearIn, Integer sizeIn)	// add cats 
		{
			_catColony.put(yearIn, sizeIn);
			
		}//method
		
		
		/*
		 * functional methods 
		 */
		
		/**
		 * getSize
		 * 
		 * <br>Provide cat colony population for a given year
		 * 
		 * @param yearIn Year equal or greater than cat colony start year
		 * @return cat colony population
		 * 
		 * @deprecated replaced by catCount
		 */
		
		protected int getSize(int yearIn)
		{			
			return catCount(yearIn);
			
		}//method

		
		/**
		 * refactor of getSize method
		 * 
		 * <br>Rename and split method
		 * 
		 * <br>catCount: count of cats in colony or -1 if year is invalid
		 * 
		 * @param yearIn year greater or equal to cat colony's founding year
		 * @return cat colony population
		 */

		protected Integer catCount(Integer yearIn)
		{
			/** refactor: move colony growth decision from year validation */
			
			boolean futureYear = get_catColonyAge() < yearIn ? true : false;
			
			if( validColonyYear(yearIn) && futureYear )
			{ 
				increaseCatColonyAge(yearIn);
				
			}//if
			
			return validColonyYear(yearIn) ? get_catColony().get(yearIn) : -1;
			
		}//method
		
		
		private boolean validColonyYear(Integer yearIn)
		{
			assertFalse("Year < colony start year", yearIn < get_yearFounded());			
			
			return yearIn >= get_yearFounded() ? true : false;
			
		}//method

		
		/**
		 * increaseCatColonyAge
		 * 
		 * <br>Increase cat colony population size per formula until requested 
		 * year
		 * 
		 * @param yearIn Future Year of colony
		 * @return true if Year valid
		 */
		
		private boolean increaseCatColonyAge(Integer yearIn)
		{
			/** refactor: reduce duplication */
			
			// validate year
			
			/*
			if( yearIn < get_yearFounded() )
			{ 
				System.out.println("Specify year >= " +	get_yearFounded());

				return false;
				
			}
			else if( yearIn <= get_catColonyAge() )
			{
				System.out.println("Specify year > " +	get_catColonyAge());
				
				return false;
				
			}//if
            */
			
			if( !validColonyYear(yearIn) ) return false;
			

			/* 
			 * Generalization: even numbered years: no growth
			 *				   odd years: population doubles
			 */
			
			for( int colonyAge = get_catColonyAge(); 
					 colonyAge <= yearIn; 
					 colonyAge++ )
			{
				
				// special case: 1st year
				
				if( colonyAge <= get_yearFounded() )
				{
					colonyAge = get_yearFounded();
					
					set_catColony( colonyAge, get_populationStartSize() );
					
				}
				else if(colonyAge % 2 == 0)	// even year
				{
					// equal to previous year, no growth
					
					set_catColony(colonyAge, 
							
							get_catColony().get( colonyAge - 1 ) );

				}
				else
				{
					// odd years, population doubles
					
					set_catColony(colonyAge,
							
							get_catColony().get( colonyAge - 1 ) * 2 );					
					
				}//if
				
			}//for
			
			return true;
			
		}//method
		
	}//class
	
}//class
