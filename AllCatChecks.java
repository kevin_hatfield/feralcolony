package sse659.project1;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * JUnit Suite Test<br>
 * Run each JUnit test and refactored version of code for same test data
 * @author Hatfield, Kevin
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({ 

   CatCheck1.class, 
   CatCheck2.class, 
   CatCheck3.class, 
   CatCheck4.class, 
   CatCheck5.class, 
   CatCheck6.class,  
   CatCheck7.class,   
   CatCheck8.class,
   CatCheck9.class,
   CatCheck11.class,
   CatCheck12.class,
   CatCheck13.class,
   CatCheck14.class,
   CatCheck15.class,
   CatCheck16.class

})

public class AllCatChecks 
{
	
}//class
