package sse659.project1;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

/**
 * CatCheck2<br><br>
 * 
 * Generates output data successfully; has some encapsulation
 * @author Hatfield, Kevin
 */

public class CatCheck2
{

	Map<String,Map<Integer,Integer>> _feralColony = 
			new HashMap<String,Map<Integer,Integer>>();  
			
	Map<Integer,Integer> _cats4Yr = new HashMap<Integer,Integer>();
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	
	@Before
	public void setUp() throws Exception 
	{
		_feralColony = new AllTests().get_feralColony();
		
		_cats4Yr = _feralColony.get("population");
		
	}//method

	
	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void test() 
	{
		//fail("Not yet implemented");
		
		checkColonySizeEachYear();
		
	}//method
	
	private void checkColonySizeEachYear()
	{

		//assertEquals( _cats4Yr.get(1), (Integer) 37 );
		//assertEquals( _cats4Yr.get(2), (Integer) 37 );		
		//assertEquals( _cats4Yr.get(3), (Integer) 74 );
		
		FeralCatColony wildCats = new FeralCatColony(37,1);
		
		assertEquals( _cats4Yr.get(1), (Integer) wildCats.getSize(1) ); 
		assertEquals( _cats4Yr.get(2), (Integer) wildCats.getSize(2) );
		assertEquals( _cats4Yr.get(3), (Integer) wildCats.getSize(3) );
		assertEquals( _cats4Yr.get(4), (Integer) wildCats.getSize(4) );
		
	}//method
	
	class FeralCatColony
	{
		int _yearFounded = 1;	// @TODO year validation
		
		Integer _populationStartSize;
		
		Map<Integer,Integer> _catColony = new HashMap<Integer,Integer>(); 
		
		FeralCatColony(Integer populationStartSize)
		{
			set_populationStartSize(populationStartSize);
			
		}//constructor

		
		FeralCatColony(Integer populationStartSize, Integer startYear)
		{
			set_yearFounded(startYear);
			
			set_populationStartSize(populationStartSize);
			
		}//constructor

		
		void set_populationStartSize(Integer sizeIn)
		{
			_populationStartSize = Math.abs(sizeIn);
			
		}//method

		
		void set_yearFounded(Integer yearIn)
		{
			_yearFounded = yearIn;
			
		}//method

		
		Integer get_yearFounded()
		{
			return _yearFounded;
			
		}//method

		
		Integer get_populationStartSize()
		{
			return _populationStartSize;
			
		}//method
		
		
		int getSize(int yearIn)
		{
			if( yearIn < get_yearFounded() )
			{ 
				System.out.println("Specify year >= " +	get_yearFounded());
				
				return -1;
				
			}//if
			
			if(get_catColonyAge()< yearIn) increaseCatColonyAge(yearIn);
			
			return get_catColony().get(yearIn);
			
		}//method

		
		Integer get_catColonyAge()
		{
			return _catColony.size();
			
		}//method
		
		
		Map<Integer,Integer> get_catColony() 
		{
			return Collections.unmodifiableMap(_catColony);
			
		}//method

		
		void set_catColony(Integer yearIn, Integer sizeIn)	// add cats 
		{
			_catColony.put(yearIn, sizeIn);
			
		}//method
		
		
		boolean increaseCatColonyAge(Integer yearIn)
		{
			// validate year
			
			if( yearIn < get_yearFounded() )
			{ 
				System.out.println("Specify year >= " +	get_yearFounded());

				return false;
				
			}
			else if( yearIn <= get_catColonyAge() )
			{
				System.out.println("Specify year > " +	get_catColonyAge());
				
				return false;
				
			}//if

			// special case: 1st year
			
			if( get_catColonyAge() < get_yearFounded() )
			{
				set_catColony( get_yearFounded(), get_populationStartSize() );
				
			}//if
			
			// 2nd year: colony doesn't increase
			
			if( yearIn == ( get_yearFounded() + 1 ) )
			{
				set_catColony( yearIn, 
						get_catColony().get( get_yearFounded() ) );
				
			}//if
			
			// 3rd year: colony doubles in size
			
			if( yearIn == ( get_yearFounded() + 2 ) )
			{
				set_catColony( yearIn, 
						get_catColony().get( get_yearFounded() )*2 );				
				
			}//method
			
			// 4th year: colony doesn't increase

			if( yearIn == ( get_yearFounded() + 3 ) )
			{
				set_catColony( yearIn, 
						get_catColony().get( get_yearFounded() + 2 ) );				
				
			}//method
			
			return true;
			
		}//method

		
	}//class
	
}//class
